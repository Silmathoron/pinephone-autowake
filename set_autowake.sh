desktop=''
period=15
waketime=30

# parse arguments

if [ "$USER" = "root" ]
then
    echo "This script must not be run as root/with sudo"
    exit 1
fi

if test $# -eq 0; then
    echo "Some arguments are required; enter -h for help"
    exit 1
fi

while getopts 'hd:p:w:' flag; do
  case "${flag}" in
    d) desktop="${OPTARG}" ;;
    p) period="${OPTARG}" ;;
    w) waketime="${OPTARG}" ;;
    *)
       echo "$0: set the PinePhone to wake up periodically"
       echo "Available arguments are:"
       echo "-d  desktop environment (either 'phosh' or 'plasma')"
       echo "-p  period for the wake-up in minutes (optional, defaults to 15 minutes)"
       echo "-w  wake duration for the wake-up in seconds (optional, defaults to 30 s)"
       exit 1 ;;
  esac
done

# check that desktop was provided

if [ "${desktop}" != "plasma" ] && [ "${desktop}" != "phosh" ]
then
    echo "-d (desktop environment) requires either 'phosh' or 'plasma'"
    exit 1
fi

# generate files

gendir="generated"

mkdir -p ${gendir}

# generate AutoWake service
# this service starts before sleep is triggered
# it executes AutoWake.sh to plan the wake-up
# and starts SuspendTimer after the wake

cat << EOF > ${gendir}/AutoWake.service
[Unit]
Description=AutoWake sleep hook
Before=sleep.target
StopWhenUnneeded=yes

[Service]
Type=exec
ExecStart=/bin/sh -c /usr/local/sbin/AutoWake.sh

[Install]
WantedBy=sleep.target
EOF

cat << EOF > ${gendir}/AutoWake.sh
systemctl stop SuspendTimer.timer
rtcwake -m no -s $((60*${period}))
systemctl start SuspendTimer.timer
EOF

# SuspendTimer
# Starts when WakeTimer executes
# Executes `waketime` after wake to put system back to sleep

cat << EOF > ${gendir}/SuspendTimer.timer
[Unit]
Description=Timer to put the system back to sleep
Requires=SuspendTimer.service

[Timer]
OnActiveSec=${waketime}
AccuracySec=1

[Install]
WantedBy=AutoWake.service
EOF

stpath="/usr/local/sbin/SuspendTimer.sh"

cat << EOF > ${gendir}/SuspendTimer.service
[Unit]
Description=Put the system back to sleep

[Service]
Type=exec
ExecStart=/bin/sh -c ${stpath}
EOF

command=""
result=""
uid=$(id -u $USER)

if [ "${desktop}" = "phosh" ]
then
    command="gdbus call -a unix:path=/run/user/$uid/bus -d org.gnome.ScreenSaver -o /org/gnome/ScreenSaver -m org.gnome.ScreenSaver.GetActive"
    result="(true,)"
else
    command="qdbus --bus unix:path=/run/user/$uid/bus org.freedesktop.ScreenSaver /ScreenSaver GetActive"
    result="true"
fi

cat << EOF > ${gendir}/SuspendTimer.sh
calling=\$(journalctl -u ModemManager -n 1 | grep -c 'accepted\|ringing-out')
audio_use=\$(XDG_RUNTIME_DIR=/run/user/$uid/ runuser -u $USER pacmd list-sink-inputs | grep -c 'state: RUNNING')
hotspot=\$(sudo iwconfig wlan0 | grep -c 'Mode:Master')

if [ \$(su $USER -c "${command}") = "${result}" ] && [ "\${audio_use}" = "0" ] && [ "\${hotspot}" = "0" ] && [ "\${calling}" = "0" ]
then
    systemctl suspend
fi
EOF

# copy files

sudo cp ${gendir}/SuspendTimer.service /etc/systemd/system/SuspendTimer.service
sudo cp ${gendir}/SuspendTimer.timer /etc/systemd/system/SuspendTimer.timer

sudo cp ${gendir}/SuspendTimer.sh ${stpath}
sudo chmod u+x ${stpath}

sudo cp ${gendir}/AutoWake.sh /usr/local/sbin/AutoWake.sh
sudo chmod u+x /usr/local/sbin/AutoWake.sh

sudo cp ${gendir}/AutoWake.service /etc/systemd/system/AutoWake.service

sudo systemctl enable SuspendTimer.timer
sudo systemctl enable AutoWake.service
