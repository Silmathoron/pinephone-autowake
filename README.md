# pinephone-autowake

Scripts to periodically (and briefly) wake the PinePhone to check emails and IMs (works on Mobian and Arch/ManjaroARM).

**Note:** the start of first the wake/suspend event may depend on your power settings if your "Automatic Suspend" delay is greater than the wake period.

**Note 2:** this does not work on PostmarketOS since the scripts use systemd, please get in touch if you know how to adapt this for pmOS.


## Requirements

**Warning:** those requirements are *not* optional and will prevent the phone from going back into suspend if missing!
If you encounter any issues with the scripts, please check first that these are installed:

* ``iwconfig``
* ``qdbus`` on PlasmaMobile
* ``gdbus`` on Phosh

``iwconfig`` is used to check whether wifi hotspot is on.
It should be already installed on Mobian; on ManjaroARM, install it via ``sudo pacman -S wireless_tools``.

The scripts also use ``dbus`` calls to check if the screen is locked or not.
This requires ``qdbus`` to work on PlasmaMobile, which can be installed on ManjaroARM via ``sudo pacman -S qt5-tools``; on Mobian, use ``sudo apt install qdbus-qt5``.
On Phosh, ``gdbus`` is required but should be available by default.


## Setting up the scripts

The timers can be set simply by running:

```shell
./set_autowake.sh -d your_de -p period_you_want -w wake_duration
```

where ``your_de`` is the desktop environment you're running (either ``phosh`` or ``plasma`` for PlasmaMobile), ``period_you_want`` is the period (in minutes) with which you want your PinePhone to wake up, and ``wake_duration`` is how long the phone stays awake (in seconds) before going back into suspend.

A restart should not be required but do restart if you see any issues the first time.


## Modifying the scripts

If you want to make some changes to the scripts, everything is in ``set_autowake.sh``:

- for the timer's properties look for ``SuspendTimer.timer``,
- the wake up call uses ``rtcwake``, look for ``AutoWake.sh``,
- the line preventing the timer to run while the phone is unlocked is in the ``command`` variable, look for ``SuspendTimer.sh``.

The services should be system services as they need root access.
On the other hand, to access the lockscreen/screensaver state, the command must be run as the user, and connected to the right bus address.


## Stop the autowake

To stop the autowake, just run ``./del_autowake.sh``.

## Acknowledgments

Original idea and code from [this post by DrPlamsa](https://forum.pine64.org/showthread.php?tid=14564&pid=98450#pid98450).
kmsgli for their help on the systemd scripts and sleep hook.
Guido for the Phosh-specific stuff.
Plasma Mobile developers for their great documentation!
